package com.example.java13springdz.filmography.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class DirectorsDTO extends GenericDTO {
    private String fio;
    private String position;
    private Set<Long> filmsIds;


}
