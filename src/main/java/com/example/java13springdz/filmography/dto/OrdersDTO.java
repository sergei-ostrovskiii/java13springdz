package com.example.java13springdz.filmography.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrdersDTO extends GenericDTO {
    private Long filmId;
    private Long userId;
    private LocalDateTime returnDate;
    private LocalDateTime rentDate;
    private Integer rentPeriod;
    private Boolean purchase;
}
