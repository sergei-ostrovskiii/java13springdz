package com.example.java13springdz.filmography.dto;

import com.example.java13springdz.filmography.model.Genre;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class FilmDTO extends GenericDTO{
    private String title;
    private String premierYear;
    private String country;
    private Genre genre;
    private Set<Long> directorsIds;
}
