package com.example.java13springdz.filmography.mapper;

import com.example.java13springdz.filmography.dto.UserDTO;
import com.example.java13springdz.filmography.model.GenericModel;
import com.example.java13springdz.filmography.model.Users;
import com.example.java13springdz.filmography.repository.OrdersRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapper<Users, UserDTO> {

    private OrdersRepository ordersRepository;

    protected UserMapper(ModelMapper modelMapper,
                         OrdersRepository ordersRepository) {
        super(modelMapper, Users.class, UserDTO.class);
        this.ordersRepository = ordersRepository;
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Users.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setUserOrders)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(UserDTO.class, Users.class)
                .addMappings(m -> m.skip(Users::setOrders)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, Users destination) {
        if (!Objects.isNull(source.getUserOrders())) {
            destination.setOrders(new HashSet<>(ordersRepository.findAllById(source.getUserOrders())));

        } else {
            destination.setOrders(Collections.emptySet());
        }
    }

    @Override
    protected void mapSpecificFields(Users source, UserDTO destination) {
        destination.setUserOrders(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Users entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getOrders())
                ? null
                : entity.getOrders().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
