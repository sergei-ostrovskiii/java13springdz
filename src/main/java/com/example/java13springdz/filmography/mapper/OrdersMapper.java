package com.example.java13springdz.filmography.mapper;

import com.example.java13springdz.filmography.dto.OrdersDTO;
import com.example.java13springdz.filmography.model.Order;
import com.example.java13springdz.filmography.repository.FilmRepository;
import com.example.java13springdz.filmography.repository.UsersRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.Set;

@Component
public class OrdersMapper
        extends GenericMapper<Order, OrdersDTO> {
    private final FilmRepository filmRepository;
    private final UsersRepository usersRepository;

    protected OrdersMapper(ModelMapper modelMapper,
                           FilmRepository filmRepository,
                           UsersRepository usersRepository) {
        super(modelMapper, Order.class, OrdersDTO.class);
        this.filmRepository = filmRepository;
        this.usersRepository = usersRepository;
    }

    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Order.class, OrdersDTO.class)
                .addMappings(m -> m.skip(OrdersDTO::setUserId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(OrdersDTO::setFilmId)).setPostConverter(toDtoConverter());

        super.modelMapper.createTypeMap(OrdersDTO.class, Order.class)
                .addMappings(m -> m.skip(Order::setUsers)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Order::setFilms)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OrdersDTO source, Order destination) {
        destination.setFilms(filmRepository.findById(source.getFilmId()).orElseThrow(() ->
                new NotFoundException("Фильма не найдено")));
        destination.setUsers(usersRepository.findById(source.getUserId()).orElseThrow(() ->
                new NotFoundException("Пользователя не найдено")));
    }

    @Override
    protected void mapSpecificFields(Order source, OrdersDTO destination) {
        destination.setUserId(source.getUsers().getId());
        destination.setFilmId(source.getFilms().getId());
    }

    @Override
    protected Set<Long> getIds(Order entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }

}
