package com.example.java13springdz.filmography.mapper;

import com.example.java13springdz.filmography.dto.DirectorsDTO;
import com.example.java13springdz.filmography.model.Directors;
import com.example.java13springdz.filmography.model.GenericModel;
import com.example.java13springdz.filmography.repository.FilmRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DirectorsMapper
        extends GenericMapper<Directors, DirectorsDTO> {
    private final FilmRepository filmRepository;

    protected DirectorsMapper(ModelMapper modelMapper,
                              FilmRepository filmRepository) {
        super(modelMapper, Directors.class, DirectorsDTO.class);
        this.filmRepository = filmRepository;
    }

    @Override
    protected void mapSpecificFields(DirectorsDTO source, Directors destination) {
        if (!Objects.isNull(source.getFilmsIds())) {
            destination.setFilms(new HashSet<>(filmRepository.findAllById(source.getFilmsIds())));
        } else {
            destination.setFilms(Collections.emptySet());
        }
    }

    @Override
    protected void mapSpecificFields(Directors source, DirectorsDTO destination) {
        destination.setFilmsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Directors entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getFilms())
                ? Collections.emptySet()
                : entity.getFilms().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Directors.class, DirectorsDTO.class)
                .addMappings(m -> m.skip(DirectorsDTO::setFilmsIds))
                .setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(DirectorsDTO.class, Directors.class)
                .addMappings(m -> m.skip(Directors::setFilms)).setPostConverter(toEntityConverter());
    }
}
