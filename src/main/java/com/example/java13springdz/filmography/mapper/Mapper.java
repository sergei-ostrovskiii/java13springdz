package com.example.java13springdz.filmography.mapper;

import com.example.java13springdz.filmography.dto.GenericDTO;
import com.example.java13springdz.filmography.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);
    D toDTO(E entity);
    List<E> toEntities(List<D> dtos);
    List<D> toDTOs(List<E> entities);
}
