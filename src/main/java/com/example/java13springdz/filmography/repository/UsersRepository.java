package com.example.java13springdz.filmography.repository;

import com.example.java13springdz.filmography.model.Users;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends GenericRepository<Users>{
}
