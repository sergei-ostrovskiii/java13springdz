package com.example.java13springdz.filmography.repository;

import com.example.java13springdz.filmography.model.Directors;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorsRepository
        extends GenericRepository<Directors> {
}
