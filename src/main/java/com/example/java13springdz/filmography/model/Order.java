package com.example.java13springdz.filmography.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Table(name = "orders")
@NoArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "default_gen", sequenceName = "orders_seq",
 allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class Order extends GenericModel {
    @ManyToOne
    @JoinColumn(name = "films_id", foreignKey = @ForeignKey(name = "FK_ORDERS_FILMS"))
    private Film films;
    @ManyToOne
    @JoinColumn(name = "users_id", foreignKey = @ForeignKey(name = "FK_ORDERS_USERS"))
    private Users users;

    @Column(name = "rent_date")
    private LocalDateTime rentDate;

    @Column(name = "rent_period")
    private Integer rentPeriod;

    @Column(name = "purchase")
    private Boolean purchase;

}
