package com.example.java13springdz.filmography.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpeAPIConfig {
    @Bean
    public OpenAPI filmography() {
        return new OpenAPI().info(new Info()
                .title("Онлайн фильмотека")
                .description("Сервис, позволяющий арендовать фильм в онлайн фильмотеке.")
                .version("v0.1")
                .license(new License().name("Apache 2.0").url("http://springdoc.org"))
                .contact(new Contact().name("Sergei A. Ostrovskii")
                        .email("volirvag132005@bk.ru")
                        .url(""))
        );
    }
}