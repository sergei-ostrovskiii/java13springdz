package com.example.java13springdz.filmography.controller;

import com.example.java13springdz.filmography.dto.FilmDTO;
import com.example.java13springdz.filmography.model.Film;
import com.example.java13springdz.filmography.service.FilmService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/films")
@Tag(name = "Фильмы",
        description = "Контроллер для работы с фильмами фильмотеки")
public class FilmController extends GenericController<Film, FilmDTO> {

    public FilmController(FilmService filmService){
        super(filmService);

    }

}
