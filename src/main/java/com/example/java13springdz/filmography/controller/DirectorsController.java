package com.example.java13springdz.filmography.controller;

import com.example.java13springdz.filmography.dto.DirectorsDTO;
import com.example.java13springdz.filmography.model.Directors;
import com.example.java13springdz.filmography.service.DirectorsService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping(value = "/directors")
@Tag(name = "Директора",
        description = "Контроллер для работы с директорами фильмотеки")
public class DirectorsController
        extends GenericController<Directors, DirectorsDTO> {
    private DirectorsService directorsService;

    public DirectorsController(DirectorsService directorsService) {
        super(directorsService);
        this.directorsService = directorsService;
    }
}
