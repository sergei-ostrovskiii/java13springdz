package com.example.java13springdz.filmography.controller;

import com.example.java13springdz.filmography.dto.OrdersDTO;
import com.example.java13springdz.filmography.model.Order;
import com.example.java13springdz.filmography.service.OrdersService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/orders")
@Tag(name = "Заказы",
        description = "Контроллер для работы с заказами фильмотеки")
public class OrdersController extends GenericController<Order, OrdersDTO>{

    public OrdersController(OrdersService ordersService){
        super(ordersService);
    }
}
