package com.example.java13springdz.filmography.service;

import com.example.java13springdz.filmography.dto.DirectorsDTO;
import com.example.java13springdz.filmography.mapper.DirectorsMapper;
import com.example.java13springdz.filmography.model.Directors;
import com.example.java13springdz.filmography.repository.DirectorsRepository;
import org.springframework.stereotype.Service;

@Service
public class DirectorsService
        extends GenericService<Directors, DirectorsDTO> {
    protected DirectorsService(DirectorsRepository directorsRepository,
                               DirectorsMapper directorsMapper) {
        super(directorsRepository, directorsMapper);
    }
}
