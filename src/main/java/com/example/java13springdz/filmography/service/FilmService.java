package com.example.java13springdz.filmography.service;

import com.example.java13springdz.filmography.dto.FilmDTO;
import com.example.java13springdz.filmography.mapper.FilmMapper;
import com.example.java13springdz.filmography.model.Film;
import com.example.java13springdz.filmography.repository.FilmRepository;
import org.springframework.stereotype.Service;

@Service
public class FilmService extends GenericService<Film, FilmDTO> {

    protected FilmService(FilmRepository filmRepository
            , FilmMapper filmMapper) {
        super(filmRepository, filmMapper);
    }

}
