package com.example.java13springdz.filmography.service;

import com.example.java13springdz.filmography.dto.GenericDTO;
import com.example.java13springdz.filmography.mapper.GenericMapper;
import com.example.java13springdz.filmography.model.GenericModel;
import com.example.java13springdz.filmography.repository.GenericRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public abstract class GenericService<T extends GenericModel, N extends GenericDTO> {
    protected final GenericRepository<T> repository;
    protected final GenericMapper<T, N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService(GenericRepository<T> repository
            , GenericMapper<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<N> listAll() {
        return mapper.toDTOs(repository.findAll());
    }

    public N getOne(Long id) {
        return mapper.toDTO(repository.findById(id).orElseThrow(()
                -> new NotFoundException("Данных по заданному id: " + id + " не найдены")));
    }

    public N create(N object) {
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public N update(N object) {
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
}
