package com.example.java13springdz.filmography.service;

import com.example.java13springdz.filmography.dto.OrdersDTO;
import com.example.java13springdz.filmography.mapper.OrdersMapper;
import com.example.java13springdz.filmography.model.Order;
import com.example.java13springdz.filmography.repository.OrdersRepository;
import org.springframework.stereotype.Service;

@Service
public class OrdersService extends GenericService<Order, OrdersDTO> {
    private OrdersRepository ordersRepository;

    protected OrdersService(OrdersRepository ordersRepository,
                            OrdersMapper ordersMapper) {
        super(ordersRepository, ordersMapper);
        this.ordersRepository = ordersRepository;
    }
}
