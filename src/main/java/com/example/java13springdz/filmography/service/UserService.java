package com.example.java13springdz.filmography.service;

import com.example.java13springdz.filmography.dto.RoleDTO;
import com.example.java13springdz.filmography.dto.UserDTO;
import com.example.java13springdz.filmography.mapper.UserMapper;
import com.example.java13springdz.filmography.model.Users;
import com.example.java13springdz.filmography.repository.UsersRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService extends GenericService<Users, UserDTO> {
    protected UserService(UsersRepository usersRepository,
                          UserMapper userMapper) {
        super(usersRepository, userMapper);
    }

    @Override
    public UserDTO create(UserDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        object.setRole(roleDTO);
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }
}
